//Export DOM7 to local variable to make it easy accessable
var $$ = Dom7;

// Smoothie chart
var chart = new SmoothieChart({maxValueScale:1.5,minValueScale:1.5,scaleSmoothing:1,minValue:0,interpolation:'linear',labels:{disabled:true},grid:{strokeStyle:'rgba(151, 153, 155, 1)',fillStyle:'transparent',verticalSections:0}}),
    canvas = document.getElementById('chart'),
    series = new TimeSeries();

// Set chart size
var width = $$('.middle').width();
var height = $$('.middle').height();
$$( "#chart" ).attr( "width", width ).attr( "height", height );

// Initial score
var btc_score = 0;
var usd_score = 100000;
var deal_amount = 10000;

// BTC change
var random;
var max_btc_change = 50; // percent
var min_btc_change = 33; // percent
var btc_change;

// BTC value
var btc_value = 10000;
var btc_value_change;
var btc_value_change_per_second;

// Trend duration
var max_trend_duration = 3;
var min_trend_duration = 1;
var trend_duration = 0;

// Feed the chart
setInterval(function() {
  if (trend_duration == 0) {
    // Random trend duration
    trend_duration = Math.floor(Math.random() * (max_trend_duration - min_trend_duration + 1)) + min_trend_duration;

    // Random value
    random = Math.random() * 2 - 1;
    // BTC change in %
    if (random > 0) {
      btc_change = random * max_btc_change;
    } else {
      btc_change = random * min_btc_change;
    }
    // BTC change in absolute terms
    btc_value_change = btc_value * btc_change / 100;
    btc_value_change_per_second = btc_value_change / trend_duration;
  }
  // Change BTC value
  btc_value += btc_value_change_per_second;

  // Limit bottom level of BTC
  if (btc_value < 500) {
    btc_value = btc_value * (1 + max_btc_change / 100);
  }

  // Limit top level of BTC
  if (btc_value > 1000000) {
    btc_value = btc_value * (1 - min_btc_change / 100);
  }

  // Show BTC value
  var btc_value_output = Math.floor(btc_value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  $$(".btc_value").text('$' + btc_value_output);

  // Step-decrease trend duration
  --trend_duration;

  // Append data
  series.append(new Date().getTime(), btc_value);

  // Show total score
  total_score = Math.round(btc_score * btc_value + usd_score);
  total_score_output = Math.floor(total_score).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  $$('.total_score_inner').text('$' + total_score_output);

  // Deal amount
  n = Math.log10(total_score / 100000);
  clicks = 10 + 10 * n;
  deal_amount_usd = total_score / clicks
  deal_amount_usd_output = Math.floor(deal_amount_usd).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  // $$('.deal_amount').text('$' + deal_amount_usd_output);
  deal_amount_btc = deal_amount_usd / btc_value;

}, 1000);

// Add data to the chart
chart.addTimeSeries(series, {lineWidth:4,strokeStyle:'#fff'});
chart.streamTo(canvas, 0);

// Buy BTC
$$('.button-buy').on('click', function () {
  if (usd_score >= deal_amount_usd) {
    btc_score += deal_amount_usd / btc_value;
    usd_score -= deal_amount_usd;
  } else {
    btc_score += usd_score / btc_value;
    usd_score = 0;
  }
  btc_score_output = Math.round(btc_score * 1000) / 1000;
  usd_score_output = Math.round(usd_score).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  $$('.btc_score_inner').text(btc_score_output);
  $$('.usd_score_inner').text(usd_score_output);
});

// Sell BTC
$$('.button-sell').on('click', function () {
  if (btc_score >= deal_amount_btc) {
    usd_score += deal_amount_btc * btc_value;
    btc_score -= deal_amount_btc;
  } else {
    usd_score += btc_score * btc_value;
    btc_score = 0;
  }
  btc_score_output = Math.round(btc_score * 1000) / 1000;
  usd_score_output = Math.round(usd_score).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  $$('.btc_score_inner').text(btc_score_output);
  $$('.usd_score_inner').text(usd_score_output);
});

// Restart game
$$('.restart').on('click', function () {
  // keep startup url (in case your app is an SPA with html5 url routing)
  // var initialHref = window.location.href;
  // Reload original app url (ie your index.html file)
  // window.location = initialHref;

  // Reset score
  btc_score = 0;
  usd_score = 100000;
  deal_amount = 10000;
  btc_value = 10000;
  total_score = 100000;
  $$('.btc_score_inner').text('0');
  $$('.usd_score_inner').text('100,000');
  $$('.total_score_inner').text('100,000');
});
